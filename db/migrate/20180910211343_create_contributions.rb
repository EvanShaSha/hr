class CreateContributions < ActiveRecord::Migration[5.0]
  def change
    create_table :contributions do |t|
      t.references :identity, foreign_key: true
      t.string :year
      t.string :month
      t.string :cutoff_day
      t.decimal :cash_investment, default: '0'
      t.decimal :salary_amount
      t.decimal :contributing_percentage, default: '1'
      t.decimal :labor_investment
      t.decimal :paying_cash
      t.text :note
      t.decimal :bonus, default: '0'
      t.boolean :approved, default: false

      t.timestamps
    end
  end
end
