class CreateIdentities < ActiveRecord::Migration[5.0]
  def change
    create_table :identities do |t|
      t.references :user, foreign_key: true
      t.string :chinese_name

      t.string :sex
      t.string :mobile_number
      t.string :c_name, default: 'PRC中华人民共和国'
      t.string :province
      t.string :city
      t.string :district
      t.string :community
      t.string :address
      t.string :zipcode
      t.date :birth_date

      t.string :status, default: 'unverified尚未验证'

      t.timestamps
    end
  end
end
