class CreateBackgrounds < ActiveRecord::Migration[5.0]
  def change
    create_table :backgrounds do |t|
      t.references :identity, foreign_key: true
      t.string :id_number
      t.string :id_type
      t.string :id_photo
      t.string :political_status
      t.boolean :home_ownership, default: false
      t.boolean :last_employer_recommendation, default: false
      t.boolean :convicted_crimes, default: true
      t.boolean :lawsuits_with_employers, default: true
      t.boolean :deadbeat_list, default: true
      t.boolean :debt, default: true
      t.boolean :handicaped, default: true
      t.boolean :vow, default: false
      t.text :explaination
      t.boolean :verified
      t.text :note
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
