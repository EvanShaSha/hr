class CreateAssessments < ActiveRecord::Migration[5.0]
  def change
    create_table :assessments do |t|
      t.references :identity, foreign_key: true
      t.references :project, foreign_key: true
      t.boolean :completed
      t.decimal :quickness
      t.decimal :quality
      t.decimal :compliance
      t.decimal :score
      t.text :note

      t.timestamps
    end
  end
end
