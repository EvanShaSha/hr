class CreateBusinesses < ActiveRecord::Migration[5.0]
  def change
    create_table :businesses do |t|
      t.references :identity, foreign_key: true
      t.string :name
      t.string :pinyin
      t.string :btype
      t.string :website
      t.string :contact
      t.string :telephone
      t.string :account
      t.string :codeword
      t.text :note

      t.timestamps
    end
  end
end
