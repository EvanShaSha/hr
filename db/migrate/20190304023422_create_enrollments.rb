class CreateEnrollments < ActiveRecord::Migration[5.0]
  def change
    create_table :enrollments do |t|
      t.references :identity, foreign_key: true
      t.string :mobile_number
      t.string :payment_method
      t.string :payment_account
      t.string :emergency_contact
      t.string :relationship
      t.string :mobile
      t.boolean :approved

      t.timestamps
    end
  end
end
