class CreateOffers < ActiveRecord::Migration[5.0]
  def change
    create_table :offers do |t|
      t.references :user, foreign_key: true
      t.references :identity, foreign_key: true
      t.string :role_name
      t.text :major_duty
      t.string :team
      t.date :starting_date
      t.string :salary_type
      t.string :salary_amount
      t.text :offer_letter
      t.boolean :accepted, default: false

      t.timestamps
    end
  end
end
