class CreateParticipants < ActiveRecord::Migration[5.0]
  def change
    create_table :participants do |t|
      t.references :identity, foreign_key: true
      t.references :project, foreign_key: true
      t.date :starting_date
      t.date :ending_date
      t.string :role
      t.text :note

      t.timestamps
    end
  end
end
