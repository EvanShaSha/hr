class CreateProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :projects do |t|
      t.references :identity, foreign_key: true
      t.string :name
      t.string :priority
      t.date :starting_date
      t.date :due_date
      t.integer :teammate_number
      t.decimal :estimated_budget
      t.text :description
      t.boolean :approved
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
