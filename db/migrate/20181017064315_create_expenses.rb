class CreateExpenses < ActiveRecord::Migration[5.0]
  def change
    create_table :expenses do |t|
      t.references :identity, foreign_key: true
      t.decimal :amount
      t.references :business, foreign_key: true
      t.string :source
      t.string :etype
      t.date :due_date
      t.date :expiration_date
      t.string :picture
      t.text :note
      t.boolean :approved
      t.references :user, foreign_key: true
      
      t.timestamps
    end
  end
end
