class CreateReferences < ActiveRecord::Migration[5.0]
  def change
    create_table :references do |t|
      t.references :identity, foreign_key: true
      t.string :fullname
      t.string :mobile
      t.string :relationship
      t.string :company_name
      t.string :company_url
      t.string :position
      t.boolean :verified
      t.text :note
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
