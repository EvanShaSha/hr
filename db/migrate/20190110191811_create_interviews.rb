class CreateInterviews < ActiveRecord::Migration[5.0]
  def change
    create_table :interviews do |t|
      t.references :identity, foreign_key: true
      t.decimal :self_starter, default: '0'
      t.decimal :community_involvement, default: '0'
      t.decimal :writting_skill, default: '0'
      t.decimal :speech_skill, default: '0'
      t.decimal :professionalism, default: '0'
      t.decimal :computer_skill, default: '0'
      t.decimal :smart_phone_skill, default: '0'
      t.decimal :internet_skill, default: '0'
      t.decimal :ruby_on_rails, default: '0'
      t.decimal :html, default: '0'
      t.decimal :css, default: '0'
      t.decimal :javascript, default: '0'
      t.text :note
      t.boolean :passed
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
