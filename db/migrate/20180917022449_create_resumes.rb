class CreateResumes < ActiveRecord::Migration[5.0]
  def change
    create_table :resumes do |t|
      t.references :identity, foreign_key: true
      t.text :cover_letter
      t.text :resume
      t.boolean :vow, default: false
      t.string :photo
      t.timestamps
    end
  end
end
