# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190304023422) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assessments", force: :cascade do |t|
    t.integer  "identity_id"
    t.integer  "project_id"
    t.boolean  "completed"
    t.decimal  "quickness"
    t.decimal  "quality"
    t.decimal  "compliance"
    t.decimal  "score"
    t.text     "note"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["identity_id"], name: "index_assessments_on_identity_id", using: :btree
    t.index ["project_id"], name: "index_assessments_on_project_id", using: :btree
  end

  create_table "backgrounds", force: :cascade do |t|
    t.integer  "identity_id"
    t.string   "id_number"
    t.string   "id_type"
    t.string   "id_photo"
    t.string   "political_status"
    t.boolean  "home_ownership",               default: false
    t.boolean  "last_employer_recommendation", default: false
    t.boolean  "convicted_crimes",             default: true
    t.boolean  "lawsuits_with_employers",      default: true
    t.boolean  "deadbeat_list",                default: true
    t.boolean  "debt",                         default: true
    t.boolean  "handicaped",                   default: true
    t.boolean  "vow",                          default: false
    t.text     "explaination"
    t.boolean  "verified"
    t.text     "note"
    t.integer  "user_id"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.index ["identity_id"], name: "index_backgrounds_on_identity_id", using: :btree
    t.index ["user_id"], name: "index_backgrounds_on_user_id", using: :btree
  end

  create_table "businesses", force: :cascade do |t|
    t.integer  "identity_id"
    t.string   "name"
    t.string   "pinyin"
    t.string   "btype"
    t.string   "website"
    t.string   "contact"
    t.string   "telephone"
    t.string   "account"
    t.string   "codeword"
    t.text     "note"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["identity_id"], name: "index_businesses_on_identity_id", using: :btree
  end

  create_table "contributions", force: :cascade do |t|
    t.integer  "identity_id"
    t.string   "year"
    t.string   "month"
    t.string   "cutoff_day"
    t.decimal  "cash_investment",         default: "0.0"
    t.decimal  "salary_amount"
    t.decimal  "contributing_percentage", default: "1.0"
    t.decimal  "labor_investment"
    t.decimal  "paying_cash"
    t.text     "note"
    t.decimal  "bonus",                   default: "0.0"
    t.boolean  "approved",                default: false
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["identity_id"], name: "index_contributions_on_identity_id", using: :btree
  end

  create_table "enrollments", force: :cascade do |t|
    t.integer  "identity_id"
    t.string   "mobile_number"
    t.string   "payment_method"
    t.string   "payment_account"
    t.string   "emergency_contact"
    t.string   "relationship"
    t.string   "mobile"
    t.boolean  "approved"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["identity_id"], name: "index_enrollments_on_identity_id", using: :btree
  end

  create_table "expenses", force: :cascade do |t|
    t.integer  "identity_id"
    t.decimal  "amount"
    t.integer  "business_id"
    t.string   "source"
    t.string   "etype"
    t.date     "due_date"
    t.date     "expiration_date"
    t.string   "picture"
    t.text     "note"
    t.boolean  "approved"
    t.integer  "user_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["business_id"], name: "index_expenses_on_business_id", using: :btree
    t.index ["identity_id"], name: "index_expenses_on_identity_id", using: :btree
    t.index ["user_id"], name: "index_expenses_on_user_id", using: :btree
  end

  create_table "identities", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "chinese_name"
    t.string   "sex"
    t.string   "mobile_number"
    t.string   "c_name",        default: "PRC中华人民共和国"
    t.string   "province"
    t.string   "city"
    t.string   "district"
    t.string   "community"
    t.string   "address"
    t.string   "zipcode"
    t.date     "birth_date"
    t.string   "status",        default: "unverified尚未验证"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.index ["user_id"], name: "index_identities_on_user_id", using: :btree
  end

  create_table "interviews", force: :cascade do |t|
    t.integer  "identity_id"
    t.decimal  "self_starter",          default: "0.0"
    t.decimal  "community_involvement", default: "0.0"
    t.decimal  "writting_skill",        default: "0.0"
    t.decimal  "speech_skill",          default: "0.0"
    t.decimal  "professionalism",       default: "0.0"
    t.decimal  "computer_skill",        default: "0.0"
    t.decimal  "smart_phone_skill",     default: "0.0"
    t.decimal  "internet_skill",        default: "0.0"
    t.decimal  "ruby_on_rails",         default: "0.0"
    t.decimal  "html",                  default: "0.0"
    t.decimal  "css",                   default: "0.0"
    t.decimal  "javascript",            default: "0.0"
    t.text     "note"
    t.boolean  "passed"
    t.integer  "user_id"
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
    t.index ["identity_id"], name: "index_interviews_on_identity_id", using: :btree
    t.index ["user_id"], name: "index_interviews_on_user_id", using: :btree
  end

  create_table "offers", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "identity_id"
    t.string   "role_name"
    t.text     "major_duty"
    t.string   "team"
    t.date     "starting_date"
    t.string   "salary_type"
    t.string   "salary_amount"
    t.text     "offer_letter"
    t.boolean  "accepted",      default: false
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.index ["identity_id"], name: "index_offers_on_identity_id", using: :btree
    t.index ["user_id"], name: "index_offers_on_user_id", using: :btree
  end

  create_table "participants", force: :cascade do |t|
    t.integer  "identity_id"
    t.integer  "project_id"
    t.date     "starting_date"
    t.date     "ending_date"
    t.string   "role"
    t.text     "note"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["identity_id"], name: "index_participants_on_identity_id", using: :btree
    t.index ["project_id"], name: "index_participants_on_project_id", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.integer  "identity_id"
    t.string   "name"
    t.string   "priority"
    t.date     "starting_date"
    t.date     "due_date"
    t.integer  "teammate_number"
    t.decimal  "estimated_budget"
    t.text     "description"
    t.boolean  "approved"
    t.integer  "user_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.index ["identity_id"], name: "index_projects_on_identity_id", using: :btree
    t.index ["user_id"], name: "index_projects_on_user_id", using: :btree
  end

  create_table "references", force: :cascade do |t|
    t.integer  "identity_id"
    t.string   "fullname"
    t.string   "mobile"
    t.string   "relationship"
    t.string   "company_name"
    t.string   "company_url"
    t.string   "position"
    t.boolean  "verified"
    t.text     "note"
    t.integer  "user_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["identity_id"], name: "index_references_on_identity_id", using: :btree
    t.index ["user_id"], name: "index_references_on_user_id", using: :btree
  end

  create_table "resumes", force: :cascade do |t|
    t.integer  "identity_id"
    t.text     "cover_letter"
    t.text     "resume"
    t.boolean  "vow",          default: false
    t.string   "photo"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.index ["identity_id"], name: "index_resumes_on_identity_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "level",                  default: 15
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  end

  add_foreign_key "assessments", "identities"
  add_foreign_key "assessments", "projects"
  add_foreign_key "backgrounds", "identities"
  add_foreign_key "backgrounds", "users"
  add_foreign_key "businesses", "identities"
  add_foreign_key "contributions", "identities"
  add_foreign_key "enrollments", "identities"
  add_foreign_key "expenses", "businesses"
  add_foreign_key "expenses", "identities"
  add_foreign_key "expenses", "users"
  add_foreign_key "identities", "users"
  add_foreign_key "interviews", "identities"
  add_foreign_key "interviews", "users"
  add_foreign_key "offers", "identities"
  add_foreign_key "offers", "users"
  add_foreign_key "participants", "identities"
  add_foreign_key "participants", "projects"
  add_foreign_key "projects", "identities"
  add_foreign_key "projects", "users"
  add_foreign_key "references", "identities"
  add_foreign_key "references", "users"
  add_foreign_key "resumes", "identities"
end
