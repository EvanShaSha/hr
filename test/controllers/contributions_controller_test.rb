require 'test_helper'

class ContributionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @contribution = contributions(:one)
  end

  test "should get index" do
    get contributions_url
    assert_response :success
  end

  test "should get new" do
    get new_contribution_url
    assert_response :success
  end

  test "should create contribution" do
    assert_difference('Contribution.count') do
      post contributions_url, params: { contribution: { approved: @contribution.approved, bonus: @contribution.bonus, cash_investment: @contribution.cash_investment, contributing_percentage: @contribution.contributing_percentage, cutoff_day: @contribution.cutoff_day, identity_id: @contribution.identity_id, labor_investment: @contribution.labor_investment, month: @contribution.month, note: @contribution.note, paying_cash: @contribution.paying_cash, salary_amount: @contribution.salary_amount, year: @contribution.year } }
    end

    assert_redirected_to contribution_url(Contribution.last)
  end

  test "should show contribution" do
    get contribution_url(@contribution)
    assert_response :success
  end

  test "should get edit" do
    get edit_contribution_url(@contribution)
    assert_response :success
  end

  test "should update contribution" do
    patch contribution_url(@contribution), params: { contribution: { approved: @contribution.approved, bonus: @contribution.bonus, cash_investment: @contribution.cash_investment, contributing_percentage: @contribution.contributing_percentage, cutoff_day: @contribution.cutoff_day, identity_id: @contribution.identity_id, labor_investment: @contribution.labor_investment, month: @contribution.month, note: @contribution.note, paying_cash: @contribution.paying_cash, salary_amount: @contribution.salary_amount, year: @contribution.year } }
    assert_redirected_to contribution_url(@contribution)
  end

  test "should destroy contribution" do
    assert_difference('Contribution.count', -1) do
      delete contribution_url(@contribution)
    end

    assert_redirected_to contributions_url
  end
end
