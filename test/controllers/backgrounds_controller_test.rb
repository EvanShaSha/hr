require 'test_helper'

class BackgroundsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @background = backgrounds(:one)
  end

  test "should get index" do
    get backgrounds_url
    assert_response :success
  end

  test "should get new" do
    get new_background_url
    assert_response :success
  end

  test "should create background" do
    assert_difference('Background.count') do
      post backgrounds_url, params: { background: { convicted_crimes: @background.convicted_crimes, deadbeat_list: @background.deadbeat_list, debt: @background.debt, explaination: @background.explaination, handicaped: @background.handicaped, home_ownership: @background.home_ownership, id_number: @background.id_number, id_photo: @background.id_photo, id_type: @background.id_type, identity_id: @background.identity_id, last_employer_recommendation: @background.last_employer_recommendation, lawsuits_with_employers: @background.lawsuits_with_employers, note: @background.note, political_status: @background.political_status, user_id: @background.user_id, verified: @background.verified, vow: @background.vow } }
    end

    assert_redirected_to background_url(Background.last)
  end

  test "should show background" do
    get background_url(@background)
    assert_response :success
  end

  test "should get edit" do
    get edit_background_url(@background)
    assert_response :success
  end

  test "should update background" do
    patch background_url(@background), params: { background: { convicted_crimes: @background.convicted_crimes, deadbeat_list: @background.deadbeat_list, debt: @background.debt, explaination: @background.explaination, handicaped: @background.handicaped, home_ownership: @background.home_ownership, id_number: @background.id_number, id_photo: @background.id_photo, id_type: @background.id_type, identity_id: @background.identity_id, last_employer_recommendation: @background.last_employer_recommendation, lawsuits_with_employers: @background.lawsuits_with_employers, note: @background.note, political_status: @background.political_status, user_id: @background.user_id, verified: @background.verified, vow: @background.vow } }
    assert_redirected_to background_url(@background)
  end

  test "should destroy background" do
    assert_difference('Background.count', -1) do
      delete background_url(@background)
    end

    assert_redirected_to backgrounds_url
  end
end
