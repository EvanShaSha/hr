require 'test_helper'

class OffersControllerTest < ActionDispatch::IntegrationTest
  setup do
    @offer = offers(:one)
  end

  test "should get index" do
    get offers_url
    assert_response :success
  end

  test "should get new" do
    get new_offer_url
    assert_response :success
  end

  test "should create offer" do
    assert_difference('Offer.count') do
      post offers_url, params: { offer: { accepted: @offer.accepted, identity_id: @offer.identity_id, major_duty: @offer.major_duty, offer_letter: @offer.offer_letter, role_name: @offer.role_name, salary_amount: @offer.salary_amount, salary_type: @offer.salary_type, starting_date: @offer.starting_date, team: @offer.team, user_id: @offer.user_id } }
    end

    assert_redirected_to offer_url(Offer.last)
  end

  test "should show offer" do
    get offer_url(@offer)
    assert_response :success
  end

  test "should get edit" do
    get edit_offer_url(@offer)
    assert_response :success
  end

  test "should update offer" do
    patch offer_url(@offer), params: { offer: { accepted: @offer.accepted, identity_id: @offer.identity_id, major_duty: @offer.major_duty, offer_letter: @offer.offer_letter, role_name: @offer.role_name, salary_amount: @offer.salary_amount, salary_type: @offer.salary_type, starting_date: @offer.starting_date, team: @offer.team, user_id: @offer.user_id } }
    assert_redirected_to offer_url(@offer)
  end

  test "should destroy offer" do
    assert_difference('Offer.count', -1) do
      delete offer_url(@offer)
    end

    assert_redirected_to offers_url
  end
end
