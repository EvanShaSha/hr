require 'test_helper'

class InterviewsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @interview = interviews(:one)
  end

  test "should get index" do
    get interviews_url
    assert_response :success
  end

  test "should get new" do
    get new_interview_url
    assert_response :success
  end

  test "should create interview" do
    assert_difference('Interview.count') do
      post interviews_url, params: { interview: { community_involvement: @interview.community_involvement, computer_skill: @interview.computer_skill, css: @interview.css, html: @interview.html, identity_id: @interview.identity_id, internet_skill: @interview.internet_skill, javascript: @interview.javascript, note: @interview.note, passed: @interview.passed, professionalism: @interview.professionalism, ruby_on_rails: @interview.ruby_on_rails, self_starter: @interview.self_starter, smart_phone_skill: @interview.smart_phone_skill, speech_skill: @interview.speech_skill, user_id: @interview.user_id, writting_skill: @interview.writting_skill } }
    end

    assert_redirected_to interview_url(Interview.last)
  end

  test "should show interview" do
    get interview_url(@interview)
    assert_response :success
  end

  test "should get edit" do
    get edit_interview_url(@interview)
    assert_response :success
  end

  test "should update interview" do
    patch interview_url(@interview), params: { interview: { community_involvement: @interview.community_involvement, computer_skill: @interview.computer_skill, css: @interview.css, html: @interview.html, identity_id: @interview.identity_id, internet_skill: @interview.internet_skill, javascript: @interview.javascript, note: @interview.note, passed: @interview.passed, professionalism: @interview.professionalism, ruby_on_rails: @interview.ruby_on_rails, self_starter: @interview.self_starter, smart_phone_skill: @interview.smart_phone_skill, speech_skill: @interview.speech_skill, user_id: @interview.user_id, writting_skill: @interview.writting_skill } }
    assert_redirected_to interview_url(@interview)
  end

  test "should destroy interview" do
    assert_difference('Interview.count', -1) do
      delete interview_url(@interview)
    end

    assert_redirected_to interviews_url
  end
end
