require 'test_helper'

class IdentitiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @identity = identities(:one)
  end

  test "should get index" do
    get identities_url
    assert_response :success
  end

  test "should get new" do
    get new_identity_url
    assert_response :success
  end

  test "should create identity" do
    assert_difference('Identity.count') do
      post identities_url, params: { identity: { address: @identity.address, c_name: @identity.c_name, chinese_name: @identity.chinese_name, city: @identity.city, community: @identity.community, district: @identity.district, mobile_number: @identity.mobile_number, pinyin_name: @identity.pinyin_name, province: @identity.province, random_code: @identity.random_code, resume: @identity.resume, sex: @identity.sex, status: @identity.status, user_id: @identity.user_id, vow: @identity.vow } }
    end

    assert_redirected_to identity_url(Identity.last)
  end

  test "should show identity" do
    get identity_url(@identity)
    assert_response :success
  end

  test "should get edit" do
    get edit_identity_url(@identity)
    assert_response :success
  end

  test "should update identity" do
    patch identity_url(@identity), params: { identity: { address: @identity.address, c_name: @identity.c_name, chinese_name: @identity.chinese_name, city: @identity.city, community: @identity.community, district: @identity.district, mobile_number: @identity.mobile_number, pinyin_name: @identity.pinyin_name, province: @identity.province, random_code: @identity.random_code, resume: @identity.resume, sex: @identity.sex, status: @identity.status, user_id: @identity.user_id, vow: @identity.vow } }
    assert_redirected_to identity_url(@identity)
  end

  test "should destroy identity" do
    assert_difference('Identity.count', -1) do
      delete identity_url(@identity)
    end

    assert_redirected_to identities_url
  end
end
