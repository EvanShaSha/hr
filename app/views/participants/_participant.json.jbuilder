json.extract! participant, :id, :identity_id, :project_id, :starting_date, :ending_date, :role, :note, :created_at, :updated_at
json.url participant_url(participant, format: :json)
