json.extract! background, :id, :identity_id, :id_number, :id_type, :id_photo, :political_status, :home_ownership, :last_employer_recommendation, :convicted_crimes, :lawsuits_with_employers, :deadbeat_list, :debt, :handicaped, :vow, :explaination, :verified, :note, :user_id, :created_at, :updated_at
json.url background_url(background, format: :json)
