json.extract! enrollment, :id, :identity_id, :mobile_number, :payment_method, :payment_account, :emergency_contact, :relationship, :mobile, :approved, :created_at, :updated_at
json.url enrollment_url(enrollment, format: :json)
