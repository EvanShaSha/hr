json.extract! offer, :id, :user_id, :identity_id, :role_name, :major_duty, :team, :starting_date, :salary_type, :salary_amount, :offer_letter, :accepted, :created_at, :updated_at
json.url offer_url(offer, format: :json)
