json.extract! identity, :id, :user_id, :chinese_name, :pinyin_name, :sex, :mobile_number, :c_name, :province, :city, :district, :community, :address, :random_code, :resume, :vow, :status, :created_at, :updated_at
json.url identity_url(identity, format: :json)
