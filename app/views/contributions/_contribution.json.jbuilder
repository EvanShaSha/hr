json.extract! contribution, :id, :identity_id, :year, :month, :cutoff_day, :cash_investment, :salary_amount, :contributing_percentage, :labor_investment, :paying_cash, :note, :bonus, :approved, :created_at, :updated_at
json.url contribution_url(contribution, format: :json)
