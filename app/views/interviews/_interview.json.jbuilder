json.extract! interview, :id, :identity_id, :self_starter, :community_involvement, :writting_skill, :speech_skill, :professionalism, :computer_skill, :smart_phone_skill, :internet_skill, :ruby_on_rails, :html, :css, :javascript, :note, :passed, :user_id, :created_at, :updated_at
json.url interview_url(interview, format: :json)
