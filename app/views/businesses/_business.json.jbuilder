json.extract! business, :id, :identity_id, :name, :pinyin, :btype, :website, :contact, :telephone, :account, :codeword, :note, :created_at, :updated_at
json.url business_url(business, format: :json)
