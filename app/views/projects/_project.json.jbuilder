json.extract! project, :id, :identity_id, :name, :priority, :starting_date, :due_date, :teammate_number, :estimated_budget, :description, :approved, :user_id, :created_at, :updated_at
json.url project_url(project, format: :json)
