json.extract! expense, :id, :identity_id, :amount, :business_id, :source, :etype, :due_date, :expiration_date, :note, :approved, :user_id, :created_at, :updated_at
json.url expense_url(expense, format: :json)
