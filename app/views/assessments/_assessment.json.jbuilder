json.extract! assessment, :id, :identity_id, :project_id, :completed, :quickness, :quality, :compliance, :score, :note, :created_at, :updated_at
json.url assessment_url(assessment, format: :json)
