json.extract! reference, :id, :identity_id, :fullname, :mobile, :relationship, :company_name, :company_url, :position, :verified, :note, :user_id, :created_at, :updated_at
json.url reference_url(reference, format: :json)
