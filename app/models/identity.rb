class Identity < ApplicationRecord
  belongs_to :user

  #has_many :offers
  # do not know why multiple offers did not work. However, you can figure out previous offers according to contributions. the same problem with references

  has_many :contributions, dependent: :destroy
  has_many :businesses, dependent: :destroy
  has_many :projects, dependent: :destroy
  has_many :participants, dependent: :destroy
  has_many :expenses, dependent: :destroy
  has_many :assessments, dependent: :destroy

  has_one :reference, dependent: :destroy
  has_one :resume, dependent: :destroy
  has_one :interview, dependent: :destroy
  has_one :background, dependent: :destroy
  has_one :offer, dependent: :destroy
  has_one :enrollment, dependent: :destroy

  validates :user_id, presence: true, uniqueness: true
  validates :chinese_name, presence: true, length: { minimum: 2, maximum: 42 }
  validates :sex, presence: true
  validates :c_name, presence: true, length: { minimum: 2, maximum: 42 }
  validates :province, presence: true, length: { minimum: 2, maximum: 42 }
  validates :city, presence: true, length: { minimum: 2, maximum: 42 }
  validates :district, presence: true, length: { minimum: 2, maximum: 42 }
  validates :community, presence: true, length: { minimum: 2, maximum: 42 }
  validates :address, presence: true, length: { minimum: 2, maximum: 42 }

  default_scope -> { order(updated_at: :desc) }

end
