class Expense < ApplicationRecord
  belongs_to :identity
  belongs_to :business
  belongs_to :user

  default_scope -> { order(created_at: :desc) }


  mount_uploader :picture, PictureUploader
  #mount_uploader :picture2, PictureUploader
  #validates :user_id,  presence: false

  validate :picture_size


  private

    # Validates the size of an uploaded picture on server side.
    def picture_size
      if picture.size > 1.megabytes
        errors.add(:picture, "请把照片缩小到1兆以下！should be less than 1MB")
      end
    end

end
