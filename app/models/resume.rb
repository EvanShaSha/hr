class Resume < ApplicationRecord

  belongs_to :identity

  validates :identity_id, presence: true, uniqueness: true
#once it is not validated, it cannot continue. Perhaps it is due to lost identity_id

  validates :cover_letter, presence: true
  validates :resume, presence: true
  validates :photo, presence: true

  validates :vow, acceptance: true


  default_scope -> { order(created_at: :desc) }

  mount_uploader :photo, PictureUploader
  #mount_uploader :picture2, PictureUploader
  #validates :user_id,  presence: false

  validate :photo_size

  private

    # Validates the size of an uploaded picture.
    def photo_size
      if photo.size > 1.megabytes
        errors.add(:photo, "请把照片缩小到1兆以下！should be less than 1MB")
      end
    end

  end
