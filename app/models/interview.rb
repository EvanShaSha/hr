class Interview < ApplicationRecord
  belongs_to :identity
  belongs_to :user

  default_scope -> { order(updated_at: :desc) }
end
