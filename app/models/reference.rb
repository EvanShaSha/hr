class Reference < ApplicationRecord
  belongs_to :identity
  belongs_to :user

  validates :user_id, presence: true
  validates :fullname, presence: true, length: { minimum: 2, maximum: 42 }
  validates :mobile, presence: true, length: { minimum: 2, maximum: 42 }
  validates :relationship, presence: true, length: { maximum: 42 }
  validates :company_name, presence: true, length: { maximum: 42 }
  validates :company_url, presence: true, length: { maximum: 42 }
  validates :position, presence: true, length: { maximum: 42 }

  default_scope -> { order(created_at: :desc) }
end
