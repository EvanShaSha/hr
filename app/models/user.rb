class User < ApplicationRecord
  # Include default devise modules. Others available are:
  #   :rememberable, and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :trackable, :validatable, :confirmable, :lockable, :timeoutable

  before_save { email.downcase! }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                  format: { with: VALID_EMAIL_REGEX },
                  uniqueness: { case_sensitive: false }
  #michael hart chapter6

  has_one :identity, dependent: :destroy

  has_many :offers
  has_many :projects
  has_many :interviews
  has_many :references
  has_many :backgrounds

  default_scope -> { order(created_at: :desc) }

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_now


    # if Rails.env.production?
    #   # No worker process in production to handle scheduled tasks
    #   devise_mailer.send(notification, self, *args).deliver_now
    # else
    #   devise_mailer.send(notification, self, *args).deliver_later
    # end
  end

  # force laid-off employee to sign-out. Not humanly possible to hit the screen within one second again and again to keep his/her sign-in status., home_help_url

  # def timeout_in
  #   if self.security_id != 2
  #     1.year
  #   else
  #     1.second
  #   end
  # end

  def timeout_in
    if self.level == 5
      1.second
    end
  end

end
