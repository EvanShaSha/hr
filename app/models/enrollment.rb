class Enrollment < ApplicationRecord
  belongs_to :identity

  validates :identity_id, presence: true, uniqueness: true

  validates :mobile_number, presence: true
  validates :payment_method, presence: true
  validates :payment_account, presence: true
  validates :emergency_contact, presence: true
  validates :relationship, presence: true

  validates :mobile, presence: true

  default_scope -> { order(created_at: :desc) }
  
end
