class Project < ApplicationRecord
  belongs_to :identity
  belongs_to :user

  has_many :participants
  has_one :assessment
end
