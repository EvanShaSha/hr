class Assessment < ApplicationRecord
  belongs_to :identity
  belongs_to :project

  before_validation do
    self.score = ((quickness.to_i + quality.to_i + compliance.to_i)/3.0).round(2)
  end
end
