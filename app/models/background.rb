class Background < ApplicationRecord
  belongs_to :identity
  belongs_to :user

  validates :identity_id, presence: true, uniqueness: true
  validates :id_number, presence: true, uniqueness: true
  validates :id_type, presence: true
  validates :id_photo, presence: true
  validates :political_status, presence: true
  validates :vow, acceptance: true

  default_scope -> { order(created_at: :desc) }
  
  mount_uploader :id_photo, PictureUploader
  #mount_uploader :picture2, PictureUploader
  #validates :user_id,  presence: false

  validate :photo_size

  private

    # Validates the size of an uploaded picture.
    def photo_size
      if id_photo.size > 1.megabytes
        errors.add(:photo, "请把照片缩小到1兆以下！should be less than 1MB")
      end
    end

end
