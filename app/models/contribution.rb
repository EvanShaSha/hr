class Contribution < ApplicationRecord
  belongs_to :identity

  before_validation do
    #if salary_amount != 0
      self.labor_investment = (salary_amount * contributing_percentage + bonus).round(2)
      self.paying_cash = salary_amount * (1 - contributing_percentage).round(2)
    #else
      #self.labor_investment = 0
      #self.paying_cash = 0
    end

    default_scope -> { order(month: :desc, cutoff_day: :desc) }
end
