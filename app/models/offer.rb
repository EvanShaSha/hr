class Offer < ApplicationRecord
  belongs_to :user
  belongs_to :identity

  validates :identity_id, presence: true, uniqueness: true
  validates :role_name, presence: true
  validates :major_duty, presence: true
  validates :team, presence: true
  validates :salary_type, presence: true
  validates :salary_amount, presence: true
  validates :offer_letter, presence: true

  default_scope -> { order(created_at: :desc) }
end
