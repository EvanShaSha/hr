class BusinessesController < ApplicationController
  before_action :set_business, only: [:show, :edit, :update, :destroy]

  # GET /businesses
  # GET /businesses.json
  def index

    if current_user.level > 19
      @businesses = Business.all
    else
      redirect_to root_url, notice: 'Access Denied! Business6342'
    end
  end

  # GET /businesses/1
  # GET /businesses/1.json
  def show

    #show has account and password, only accessable with raw SQL.

    if current_user.identity.nil?
      redirect_to root_url, notice: 'Access Denied! Business1766'

    elsif current_user.level > 19 && current_user.identity.id == @business.identity_id
      @business = Business.find(params[:id])

      #if level is big enough, one can only see herself or himself
    else
      redirect_to root_url, notice: 'Access Denied! Business34275'
    end
  end

  # GET /businesses/new
  def new

    if current_user.level < 20
      redirect_to root_url, notice: 'Access Denied! Business675'
    else

      @business = Business.new
    end
  end

  # GET /businesses/1/edit
  def edit
    if current_user.level > 19 && current_user.identity.id == @business.identity_id
      @business = Business.find(params[:id])

      #if level is big enough, one can only see herself or himself
    else
      redirect_to root_url, notice: 'Access Denied! Business34275'
    end
  end

  # POST /businesses
  # POST /businesses.json
  def create
    @business = Business.new(business_params)

    respond_to do |format|
      if @business.save
        format.html { redirect_to @business, notice: 'Business was successfully created.' }
        format.json { render :show, status: :created, location: @business }
      else
        format.html { render :new }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /businesses/1
  # PATCH/PUT /businesses/1.json
  def update
    respond_to do |format|
      if @business.update(business_params)
        format.html { redirect_to @business, notice: 'Business was successfully updated.' }
        format.json { render :show, status: :ok, location: @business }
      else
        format.html { render :edit }
        format.json { render json: @business.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /businesses/1
  # DELETE /businesses/1.json
  def destroy
    @business.destroy
    respond_to do |format|
      format.html { redirect_to businesses_url, notice: 'Business was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_business
      @business = Business.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def business_params
      params.require(:business).permit(:identity_id, :name, :pinyin, :btype, :website, :contact, :telephone, :account, :codeword, :note)
    end
end
