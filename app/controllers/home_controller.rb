class HomeController < ApplicationController
  before_action :authenticate_user!, :except => [:letter_short, :help, :news]
    #password protected static pages. except...
    
  def index
  end

  def letter_short
  end

  def help
  end

  def letter
  end

  def news
  end

end
