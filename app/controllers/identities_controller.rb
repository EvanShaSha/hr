class IdentitiesController < ApplicationController
  before_action :set_identity, only: [:show, :edit, :update, :destroy]

  # GET /identities
  # GET /identities.json
  def index
    if current_user.level > 19
      @identities = Identity.all
    else
      redirect_to root_url, notice: 'Access Denied! Identity6342'
    end

  end

  # GET /identities/1
  # GET /identities/1.json
  def show

    if current_user.level > 19
      @identity = Identity.find(params[:id])

    elsif @identity.user_id && @identity.user_id == current_user.id
      @identity = Identity.find(params[:id])

    else
      redirect_to root_url, notice: 'Access Denied! Identity34275'
    end
  end

  # GET /identities/new
  def new
    if params[:id].nil?
      redirect_to root_url, notice: 'Access Denied! Identity11242'
      #do not show sepecific message so an intruder knows what was wrong.

    elsif current_user.identity.nil?
      @identity = Identity.new

    else
      redirect_to root_url, notice: 'Access Denied! Identity3342'

    end
  end

  # GET /identities/1/edit
  def edit
    if current_user.level > 79
      @identity = Identity.find(params[:id])
    else
      redirect_to root_url, notice: 'Access Denied! Identity7342'
    end
  end

  # POST /identities
  # POST /identities.json
  def create
    @identity = Identity.new(identity_params)

    respond_to do |format|
      if @identity.save
        format.html { redirect_to @identity, notice: 'Identity was successfully created.' }
        format.json { render :show, status: :created, location: @identity }
      else
        format.html { render :new }
        format.json { render json: @identity.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /identities/1
  # PATCH/PUT /identities/1.json
  def update
    respond_to do |format|
      if @identity.update(identity_params)
        format.html { redirect_to @identity, notice: 'Identity was successfully updated.' }

        format.json { render :show, status: :ok, location: @identity }
      else
        format.html { render :edit }
        format.json { render json: @identity.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /identities/1
  # DELETE /identities/1.json
  def destroy
    @identity.destroy
    respond_to do |format|
      format.html { redirect_to identities_url, notice: 'Identity was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_identity
      @identity = Identity.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def identity_params
      params.require(:identity).permit(:user_id, :chinese_name, :pinyin_name, :sex, :mobile_number, :c_name, :province, :city, :district, :community, :address, :zipcode, :birth_date, :status)
    end
end
