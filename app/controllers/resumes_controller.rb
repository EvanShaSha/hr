class ResumesController < ApplicationController
  before_action :set_resume, only: [:show, :edit, :update, :destroy]

  # GET /resumes
  # GET /resumes.json
  def index

    if current_user.level > 19
      @resumes = Resume.all
      #level 20 is a working partner
    else
      redirect_to root_url, notice: 'Access Denied! Resume142'
    end
  end

  # GET /resumes/1
  # GET /resumes/1.json
  def show

    if current_user.identity.nil?
      redirect_to root_url, notice: 'Access Denied! Resume11267'

    else

      if current_user.level > 19
        @resume = Resume.find(params[:id])

      elsif @resume.identity_id == current_user.identity.id
        @resume = Resume.find(params[:id])
      #if level is big enough, one can only see herself or himself
      else
        redirect_to root_url, notice: 'Access Denied! Resume22267'
      end
      #redirect_to root_url, notice: 'Access Denied! Resume34267'
    end
  end

  # GET /resumes/new
  def new

    if params[:id].nil?
      redirect_to root_url, notice: 'Access Denied! Resume1142'
      #do not show sepecific message so an intruder knows what was wrong.
    else
      @resume = Resume.new

    end

  end

  # GET /resumes/1/edit
  # def edit
  # no editing on resumes
  # end

  # POST /resumes
  # POST /resumes.json
  def create
    @resume = Resume.new(resume_params)

    respond_to do |format|
      if @resume.save
        format.html { redirect_to @resume, notice: 'Resume was successfully created.' }
        format.json { render :show, status: :created, location: @resume }
      else
        format.html { render :new }
        format.json { render json: @resume.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /resumes/1
  # PATCH/PUT /resumes/1.json
  def update
    respond_to do |format|
      if @resume.update(resume_params)
        format.html { redirect_to @resume, notice: 'Resume was successfully updated.' }
        format.json { render :show, status: :ok, location: @resume }
      else
        format.html { render :edit }
        format.json { render json: @resume.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /resumes/1
  # DELETE /resumes/1.json
  def destroy
    @resume.destroy
    respond_to do |format|
      format.html { redirect_to resumes_url, notice: 'Resume was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_resume
      @resume = Resume.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def resume_params
      params.require(:resume).permit(:identity_id, :cover_letter, :resume, :vow, :photo)
    end
end
