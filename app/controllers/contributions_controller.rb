class ContributionsController < ApplicationController
  before_action :set_contribution, only: [:show, :edit, :update, :destroy]

  # GET /contributions
  # GET /contributions.json
  def index

    if params[:id].nil? && current_user.level <= 19
      redirect_to root_url, notice: 'Access Denied! contributions76223'

    elsif params[:id].nil? && current_user.level > 19
      @contributions = Contribution.all
      @cash_investment_sum = Contribution.where("approved = 'true'").sum("cash_investment")
      #@cash_at_bank = @cash_investment_sum - $expense_sum_dollar
      @labor_investment_sum = Contribution.where("approved = 'true'").sum("labor_investment")
      @contribution_value = @cash_investment_sum*4 + @labor_investment_sum*2

    elsif current_user.level >= 15

      #level 20 is a working partner.
    else
      redirect_to root_url, notice: 'Access Denied! contributions46223'
    end
  end

def index_individual

  if params[:id].nil?
    redirect_to root_url, notice: 'Please enter your personal information first.请填写自己的私人信息！'
  elsif current_user.level <= 19
    redirect_to root_url, notice: 'Please wait until security level upgraded请耐心等待安全系数升级！'
  #elsif @contributions.nil? && current_user.level > 19
  #  redirect_to contributions_path, notice: 'You do not have a contribution yet, please enter one.您还没有贡献，请添加！'
  else
    @contributions = Contribution.where("identity_id = ?", params[:id])

    @ind_cash_investment_sum = Contribution.where("approved = ? AND identity_id = ?", true, params[:id]).sum("cash_investment")
    @ind_labor_investment_sum = Contribution.where("approved = ? AND identity_id = ?", true, params[:id]).sum("labor_investment")
    @ind_contribution_value = @ind_cash_investment_sum*4 + @ind_labor_investment_sum*2

    @cash_investment_sum = Contribution.where("approved = 'true'").sum("cash_investment")
    @labor_investment_sum = Contribution.where("approved = 'true'").sum("labor_investment")
    @contribution_value = @cash_investment_sum*4 + @labor_investment_sum*2

    @stock_percentage = @ind_contribution_value / @contribution_value
  end

end

  # GET /contributions/1
  # GET /contributions/1.json
  def show

    if current_user.identity.nil?
      redirect_to root_url, notice: 'Access Denied! Contribution1298'

    elsif current_user.level > 19
      @contribution = Contribution.find(params[:id])
      #if level is 20, you can see everyone's contributions

    else
      redirect_to root_url, notice: 'Access Denied! Contribution34275'
    end

  end

  # GET /contributions/new
  def new

    if params[:id].nil?
      redirect_to root_url, notice: 'No ID Errors! Contribution342'
      #do not show sepecific message so an intruder knows what was wrong.

    elsif current_user.level < 20
      redirect_to root_url, notice: 'Access Denied! Contribution675'
    else

      @contribution = Contribution.new
    end
  end

  # GET /contributions/1/edit
  def edit

    if current_user.level > 79 && current_user.identity.id != @contribution.identity_id
      @contribution = Contribution.find(params[:id])

      #if level is big enough, one can only see herself or himself
    else
      redirect_to root_url, notice: 'Access Denied! Contribution34275'
    end
  end

  # POST /contributions
  # POST /contributions.json
  def create
    @contribution = Contribution.new(contribution_params)

    respond_to do |format|
      if @contribution.save
        format.html { redirect_to @contribution, notice: 'Contribution was successfully created.' }
        format.json { render :show, status: :created, location: @contribution }
      else
        format.html { render :new }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /contributions/1
  # PATCH/PUT /contributions/1.json
  def update
    respond_to do |format|
      if @contribution.update(contribution_params)
        format.html { redirect_to contributions_url, notice: 'Contribution was successfully updated.' }
        format.json { render :show, status: :ok, location: @contribution }
      else
        format.html { render :edit }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contributions/1
  # DELETE /contributions/1.json
  def destroy
    @contribution.destroy
    respond_to do |format|
      format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contribution
      @contribution = Contribution.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contribution_params
      params.require(:contribution).permit(:identity_id, :year, :month, :cutoff_day, :cash_investment, :salary_amount, :contributing_percentage, :labor_investment, :paying_cash, :note, :bonus, :approved)
    end
end
