class ExpensesController < ApplicationController
  before_action :set_expense, only: [:show, :edit, :update, :destroy]

  # GET /expenses
  # GET /expenses.json
  def index

    if current_user.level > 19
      @expenses = Expense.all
      $expense_sum = Expense.where("approved = 'true'").sum("amount")
      $expense_sum_dollar = ($expense_sum/6.8).round(2)
    else
      redirect_to root_url, notice: 'Access Denied! expense46223'
    end

  end

  # GET /expenses/1
  # GET /expenses/1.json
  def show

    if current_user.identity.nil?
      redirect_to root_url, notice: 'Access Denied! Expense1199s'

    elsif  current_user.level > 19
      @expense = Expense.find(params[:id])

      #if level is big enough, one can only see herself or himself
    else
      redirect_to root_url, notice: 'Access Denied! Expense3194'
    end
  end

  # GET /expenses/new
  def new

    if params[:id].nil?
      redirect_to root_url, notice: 'No ID Errors! Expense342'
      #do not show sepecific message so an intruder knows what was wrong.

    elsif current_user.level < 20
      redirect_to root_url, notice: 'Access Denied! Expense675'
    else

      @expense = Expense.new
    end
  end

  # GET /expenses/1/edit
  def edit
    if current_user.level < 90
      redirect_to root_url, notice: 'Access Denied! expense4198.'
    else
      @expense = Expense.find(params[:id])
    end
  end

  # POST /expenses
  # POST /expenses.json
  def create
    @expense = Expense.new(expense_params)

    respond_to do |format|
      if @expense.save
        format.html { redirect_to @expense, notice: 'Expense was successfully created.' }
        format.json { render :show, status: :created, location: @expense }
      else
        format.html { render :new }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /expenses/1
  # PATCH/PUT /expenses/1.json
  def update
    respond_to do |format|
      if @expense.update(expense_params)
        format.html { redirect_to @expense, notice: 'Expense was successfully updated.' }
        format.json { render :show, status: :ok, location: @expense }
      else
        format.html { render :edit }
        format.json { render json: @expense.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /expenses/1
  # DELETE /expenses/1.json
  def destroy
    @expense.destroy
    respond_to do |format|
      format.html { redirect_to expenses_url, notice: 'Expense was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_expense
      @expense = Expense.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def expense_params
      params.require(:expense).permit(:identity_id, :amount, :business_id, :source, :etype, :due_date, :expiration_date, :note, :approved, :user_id, :picture)

      #params.require(:expense).permit!
    end
end
