class OffersController < ApplicationController
  before_action :set_offer, only: [:show, :edit, :update, :destroy]

  # GET /offers
  # GET /offers.json
  def index
    @offers = Offer.all
    # if current_user.level < 16
    #   redirect_to root_url, notice: 'Access Denied! Offer46987'
    #
    # elsif current_user.level = 16 && current_user.offers
    #
    #   @offers = Offer.where("identity_id = ?", params[:id])
    #   #level 20 is a working partner.
    # else
    #   redirect_to root_url, notice: 'Access Denied! Offer46223'
    # end
  end


  # GET /offers/1
  # GET /offers/1.json
  def show
    if current_user.level > 79 or current_user.id == @offer.identity.user.id
      @offer = Offer.find(params[:id])

    elsif current_user.identity.id == @offer.identity_id
      @offer = Offer.find(params[:id])

    else
      redirect_to root_url, notice: 'Access Denied! offer4223'
    end
  end

  # GET /offers/new
  def new
    if params[:id].nil?
      redirect_to root_url, notice: 'Access Denied! offer34239'
      #do not show sepecific message so an intruder knows what was wrong.
    elsif current_user.level < 80
      redirect_to root_url, notice: 'Access Denied! offer84239'

    # offer is made by HR or Management only

    else

      @offer = Offer.new
    end

  end

  # GET /offers/1/edit
  def edit

    if current_user.identity.nil?
      redirect_to root_url, notice: 'Access Denied! Offer178'

    elsif @offer.identity_id != current_user.identity.id
      redirect_to root_url, notice: 'Access Denied! Offer280'
      #one can only edit herself or himself to acepted the offer, default is false.j

    elsif @offer.accepted == true
      redirect_to root_url, notice: 'Access Denied! Offer378'
      #if offer is accepted, you can not accept it again.
    else

      @offer = Offer.find(params[:id])
    end
  end

  # POST /offers
  # POST /offers.json
  def create
    @offer = Offer.new(offer_params)

    respond_to do |format|
      if @offer.save
        format.html { redirect_to @offer, notice: 'Offer was successfully created.' }
        format.json { render :show, status: :created, location: @offer }
      else
        format.html { render :new }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offers/1
  # PATCH/PUT /offers/1.json
  def update
    respond_to do |format|
      if @offer.update(offer_params)
        format.html { redirect_to @offer, notice: 'Offer was successfully updated.' }
        format.json { render :show, status: :ok, location: @offer }
      else
        format.html { render :edit }
        format.json { render json: @offer.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offers/1
  # DELETE /offers/1.json
  def destroy
    @offer.destroy
    respond_to do |format|
      format.html { redirect_to offers_url, notice: 'Offer was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer
      @offer = Offer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_params
      params.require(:offer).permit(:user_id, :identity_id, :role_name, :major_duty, :team, :starting_date, :salary_type, :salary_amount, :offer_letter, :accepted)
    end
end
