class ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :edit, :update, :destroy]

  # GET /projects
  # GET /projects.json
  def index
    if current_user.level < 20
      redirect_to root_url, notice: 'Access Denied! Project635'
    else
      @projects = Project.all
    end
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
    if current_user.level < 20
      redirect_to root_url, notice: 'Access Denied! Project639'
    else
      @project = Project.find(params[:id])
    end
  end

  # GET /projects/new
  def new

    if current_user.level < 20
      redirect_to root_url, notice: 'Access Denied! Project675'
    else
      @project = Project.new
    end

  end


  # GET /projects/1/edit
  def edit
    if current_user.level != 99
      redirect_to root_url, notice: 'Access Denied! Project4198.'
    else
      @project = Project.find(params[:id])
    end
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:identity_id, :name, :priority, :starting_date, :due_date, :teammate_number, :estimated_budget, :description, :approved, :user_id)
    end
end
