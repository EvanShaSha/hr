1. turbolinks 5.1.1 does not work well with devise 4.4.3. I had to disable it in application.js. This got fixed already.

2. if icon is bold and black, file name ends with "-filled.png", else with "-outline.png"
"-filled.png" means clickable. "red-outline.png" is for show purpose and unclickable. "plus-filled.png" always goes with new page. "forward-arrow-filled.png" means there is something step require user to do.

3. error messages are vague so that an intruder would not know why he got access denied.

-------------------------------------------------------------------------
picture_uploader.rb

if MiniMagick cut image to 100 X 100, no matter width or length will not exceed 100 pixels.

The server will process the picture and only save the picture by 100 X 100, instead of the original large picture.

uploader controls file type with a white-list, while individual model controls file size. I wonder if individual model can control file type with a white-list as well. Tried many times and failed.

is simple_form validation come from server side or client side? I think it is on server side.

Finally, I allow jpg jpeg png pdf txt xls xlsx in picture_uploader.rb so it works for multiple model. This is inflexible if you want to only store picture by 100 X 100. It is simple and easy with one uploader for now, but inflexible in the near future.

A user can only upload one single file for one single model so far. If you want to upload multiple files to one single model, says expense model, then there is more work involved with accepts_nested_attributes_for
https://kolosek.com/carrierwave-upload-multiple-images/
https://github.com/carrierwaveuploader/carrierwave/blob/master/README.md#multiple-file-uploads

I wonder if an association model for multiple pictures following another model can work. I will try in xq234_version3.

---------------------------------------------------------------------------

As soon as we get more and more applicants, we might want to separate our HR APP from our Internal Office APP due to the security concern. Yes, making the HR APP independent for future project. And HR APP allows discussion between part-timers.

If HR APP accepts someone as partner, then we issue a company email address for her/him. Internal Office APP might only allow company email address for logging in. If she/he eventually leave the company, all we need to do is deleting her/his company email address.

I wonder if contribution model should be in both of two APP.
------------------------------------------------------------------------------------------------------------------------------------------

Why do we use heroku.com as our "Internal Office" server?

PRO:
1. We can piggyback on heroku's SSL security system for free.

2. It should be more secure since we can easily create a brand new domain name once a hacker has found our IP address. This means we can move our office as soon as a break-in has taken place.

3. Our partners will reside all over the world, outside of "Great Chinese Firewall". Chinese law allows company's employees data hosted outside of wall for as long as a employee has signed an agreement.

4. It can be easily setup and go. No LINUX administrator required.

CON:
1. It is very slow if access from inside of China. However, our partners in China MUST use it no mater of how slow it is. A CDN server might be a good solution.

2. Fee might be high as we grow bigger.

----------------------------------------------------------------------------

A hiring process flowchart is in .xml file format. You can open and edit it on https://www.draw.io/

--------------------------------------------------------------------------------------------
