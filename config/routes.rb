Rails.application.routes.draw do
  resources :enrollments
  resources :backgrounds
  resources :references
  resources :interviews
  resources :assessments
  resources :participants
  resources :projects
  resources :expenses
  resources :businesses
  resources :resumes

  resources :contributions do
    member do
      get 'index_individual'
    end
  end

  resources :offers

  resources :identities

  get 'home/dsds'
  get 'home/news'
  get 'home/about'
  get 'home/help'
  get 'home/letter'
  get 'home/letter_short'
  root 'home#index'

  devise_for :users

end
