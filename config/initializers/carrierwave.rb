# if Rails.env.production?
#   CarrierWave.configure do |config|
#
#     #Some users have reported having to include the additional line
#     config.fog_provider = 'fog/aws'
#     config.fog_credentials = {
#       # Configuration for Amazon S3
#       :provider              => 'AWS',
#       :aws_access_key_id     => 'AKIASTEZCGPCQDZPQ6E2',
#       :aws_secret_access_key => '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',
#       :region                => 'us-east-2',
#       :host                  => 's3.us-east-2.amazonaws.com'
#     }
#     config.fog_directory     =  'dynamic-stock-distribution-system'
#
#   end
# end

if Rails.env.production?
  CarrierWave.configure do |config|

    #Some users have reported having to include the additional line
    config.fog_provider = 'fog/aws'
    config.fog_credentials = {
      # Configuration for Amazon S3
      :provider              => 'AWS',
      :aws_access_key_id     => ENV['AWS_ACCESS_KEY_ID'],
      :aws_secret_access_key => ENV['AWS_SECRET_ACCESS_KEY'],
      :region                => 'us-east-2',
      :host                  => 's3.us-east-2.amazonaws.com'
    }
    config.fog_directory     =  ENV['S3_BUCKET_NAME']

  end
end
